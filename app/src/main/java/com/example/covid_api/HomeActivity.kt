package com.example.covid_api

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.PopupMenu
import android.widget.TextView
import com.example.covid_api.fragment.CountryFragment
import com.example.covid_api.fragment.HomeFragment
import com.example.covid_api.model.AboutActivity
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit  var home_txt:TextView
    lateinit var btn_menu:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportActionBar!!.hide()

        home_txt = findViewById(R.id.home_activity_txt_title_id)

//        home_txt.typeface = Typeface.createFromFile()
        val homefragment = HomeFragment()
        var fragmenttransition = supportFragmentManager.beginTransaction()
        fragmenttransition.replace(R.id.home_activity_frame_layout_id , homefragment)
        fragmenttransition.commit()
        home_txt.text = "صفحه اصلی"

        var home_nav = findViewById<BottomNavigationView>(R.id.home_activity_bttom_id)

        home_nav.setOnNavigationItemSelectedListener(this)

        // اینیشیال کردن منو و ایجاد پاپ آپ
        btn_menu =findViewById(R.id.about_menu_home_id)
        btn_menu.setOnClickListener {

            val popupMenu:PopupMenu = PopupMenu(this,btn_menu)
            popupMenu.menuInflater.inflate(R.menu.about_menu,popupMenu.menu)
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener {

                when(it.itemId){

                    R.id.about_menu_id -> {

                        val intent = Intent(this,AboutActivity::class.java)
                        startActivity(intent)

                    }
                }
            true
            })

            popupMenu.show()
        }


    }


    // ست کردن تول بار با آیتم ها
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var id = item.itemId

        when(id){

            R.id.menu_home_id -> {

                val homefragment = HomeFragment()
                var fragmenttransition = supportFragmentManager.beginTransaction()
                fragmenttransition.replace(R.id.home_activity_frame_layout_id , homefragment)
                .commit()
                home_txt.text = "صفحه اصلی"

            }

            R.id.menu_country_id -> {

                var contryfragment = CountryFragment()
                var fragmenttransition = supportFragmentManager.beginTransaction()
                fragmenttransition.replace(R.id.home_activity_frame_layout_id , contryfragment)
                .commit()
                home_txt.text = "کشورها"
            }
        }

        return true
    }
}