package com.example.covid_api

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry


@Suppress("DEPRECATION")
class ChartActivity : AppCompatActivity() {

    lateinit var bundle: Bundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart)
        supportActionBar!!.hide()
        // دریافت ای دی اسم کشور و دکمه بازگشت
        val namecountry = findViewById<TextView>(R.id.chart_country_name_id)
        val back =findViewById<TextView>(R.id.country_chart_back_id)

        back.setOnClickListener {
            finish()
        }


        // قرار دادن اسم کشور با کشور بانتخاب شده
        bundle = intent.extras!!
        namecountry.text =bundle.getString("name")

        getdialypiechart()

        gettotalpiechart()

    }


    // گرفتن آمار روزانه کرونا و نشان دادن آن داخل pie chart
    fun getdialypiechart(){

        val piechart:PieChart = findViewById(R.id.pie_chart_daily_id)
        val listPie = ArrayList<PieEntry>()
        val listColors = ArrayList<Int>()
        listPie.add(PieEntry(bundle.getString("NewConfirmed")!!.toFloat(), "مبتلایان امروز"))
        listColors.add(resources.getColor(R.color.color_result_pass))
        listPie.add(PieEntry(bundle.getString("NewRecovered")!!.toFloat(), "بهبودیافتگان امروز"))
        listColors.add(resources.getColor(R.color.color_result_fail))
        listPie.add(PieEntry(bundle.getString("NewDeaths")!!.toFloat(), "فوت شدگان امروز"))
        listColors.add(resources.getColor(R.color.color_result_death))

        val pieDataSet = PieDataSet(listPie, "")
        pieDataSet.colors = listColors

        val pieData = PieData(pieDataSet)
        pieData.setValueTextSize(14f)
        piechart.data = pieData



        piechart.setEntryLabelTextSize(8f)
        piechart.isDrawHoleEnabled = false
        piechart.description.isEnabled = false
        piechart.setEntryLabelColor(R.color.black_700)
        piechart.animateY(1400, Easing.EaseInOutQuad)
    }

    // گرفتن آمار کل کرونا و نشان دادن آن داخل pie chart
    fun gettotalpiechart(){

        val piechart2:PieChart = findViewById(R.id.pie_chart_total_id)
        val listPie2 = ArrayList<PieEntry>()
        val listColors2 = ArrayList<Int>()
        listPie2.add(PieEntry(bundle.getString("TotalConfirmed")!!.toFloat(), "کل مبتلایان"))
        listColors2.add(resources.getColor(R.color.color_result_pass))
        listPie2.add(PieEntry(bundle.getString("TotalRecovered")!!.toFloat(), "کل بهبودیافتگان"))
        listColors2.add(resources.getColor(R.color.color_result_fail))
        listPie2.add(PieEntry(bundle.getString("TotalDeaths")!!.toFloat(), "کل فوت شدگان"))
        listColors2.add(resources.getColor(R.color.color_result_death))

        val pieDataSet = PieDataSet(listPie2, "")
        pieDataSet.colors = listColors2

        val pieData2 = PieData(pieDataSet)
        pieData2.setValueTextSize(14f)
        piechart2.data = pieData2



        piechart2.setEntryLabelTextSize(8f)
        piechart2.isDrawHoleEnabled = false
        piechart2.description.isEnabled = false
        piechart2.setEntryLabelColor(R.color.black_700)
        piechart2.animateY(1400, Easing.EaseInOutQuad)
    }



}