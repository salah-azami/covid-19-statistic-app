 package com.example.covid_api.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.covid_api.R
import com.example.covid_api.aapter.CountryAdapter
import com.example.covid_api.model.Countris
import com.github.ybq.android.spinkit.SpinKitView
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import java.util.*


 class CountryFragment : Fragment() {


    var base_url = "https://api.covid19api.com/summary"

    lateinit var recycler_view:RecyclerView
    lateinit var linear:LinearLayout
    lateinit var spin_kit:SpinKitView
    lateinit var countryadapter:CountryAdapter
    lateinit var search:EditText
    lateinit var volleyrequest:RequestQueue
     var listcountry:ArrayList<Countris> = ArrayList()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_country, container, false)

        listcountry.clear()

        // گرفتن آیتم ها به وسیله id
        recycler_view = view.findViewById(R.id.country_recycler_id)
        linear = view.findViewById(R.id.country_linear_id)
        spin_kit = view.findViewById(R.id.country_spin_kit)
        search = view.findViewById(R.id.search_id)

        bindingrecycler()

        volleyrequest = Volley.newRequestQueue(context)

        getjsonarray()

        search.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


            }

            override fun afterTextChanged(s: Editable?) {

                filter(s.toString())
            }

        })

        return view
    }

     // گرفتن لیستی از دیتا ها و پاس دادن آنها داخل recyclerview
     fun getjsonarray() {


        val jsonrequest = object :JsonObjectRequest(Request.Method.GET,
        base_url,null,
        Response.Listener {
            response: JSONObject ->

            val countryarray = response.getJSONArray("Countries")

            Log.d("======",countryarray.toString())
            try {

                for (i in 0 until countryarray.length()){

                    val contryobject = countryarray.getJSONObject(i)
                    val gson = Gson().fromJson(contryobject.toString() , Countris::class.java)

                    listcountry.add(gson)


                }

                countryadapter.notifyItemRangeChanged(listcountry.count() , listcountry.count())

                linear.visibility = View.VISIBLE
                spin_kit.visibility = View.GONE

            }catch (e:JSONException){
                e.printStackTrace()
            }

        },
        Response.ErrorListener {  })
        {

        }

        volleyrequest.add(jsonrequest)
    }

     // اتصال recyclerviewبه view
     fun bindingrecycler(){

         countryadapter = CountryAdapter(context!!,listcountry)
         recycler_view.adapter = countryadapter
         recycler_view.layoutManager = LinearLayoutManager(context)
     }

     //فیلتر کردن داده ها بر اساس نام
     fun filter(text:String){


         val filteredlist:ArrayList<Countris> = ArrayList()

         for (item:Countris in listcountry){

             if (item.Country.toLowerCase().contains(text.toLowerCase()))
                 filteredlist.add(item)

         }

         countryadapter.filterlist(filteredlist)

     }
}


