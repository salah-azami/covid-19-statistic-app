package com.example.covid_api.fragment

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.covid_api.R
import com.github.ybq.android.spinkit.SpinKitView
import org.json.JSONException
import java.text.DecimalFormat

class HomeFragment : Fragment() {

    var baseurl = "https://api.covid19api.com/summary"
    var volleyrequest:RequestQueue? = null
    lateinit var NewConfirmed:TextView
    lateinit var TotalConfirmed:TextView
    lateinit var NewDeaths:TextView
    lateinit var TotalDeaths:TextView
    lateinit var NewRecovered:TextView
    lateinit var TotalRecovered:TextView
    lateinit var paren:LinearLayout
    lateinit var spnkit:SpinKitView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_home, container, false)

         NewConfirmed = view.findViewById(R.id.home_NewConfirmed_tod_id)
         TotalConfirmed = view.findViewById(R.id.home_TotalConfirmed_id)
         NewDeaths = view.findViewById(R.id.home_NewDeaths_tod_id)
         TotalDeaths = view.findViewById(R.id.home_TotalDeaths_id)
         NewRecovered = view.findViewById(R.id.home_NewRecovered_tod_id)
         TotalRecovered = view.findViewById(R.id.home_TotalRecovered_id)
        val linktext = view.findViewById<TextView>(R.id.link_id)

        paren = view.findViewById(R.id.parent_id)
        spnkit = view.findViewById(R.id.spin_kit)

        volleyrequest = Volley.newRequestQueue(context)

        linktext.setLinkTextColor(Color.BLUE)


        getstring()

        return view

    }


    @SuppressLint("SetTextI18n")
    fun getstring(){


        val jsonobject = JsonObjectRequest(Request.Method.GET,
                baseurl,
                null,
                {


                    Response -> try {



                    val global = Response.getJSONObject("Global")
                    val decimalformat = DecimalFormat("###,###")

                    val NewConfirmed_ = global.getInt("NewConfirmed")
                    val newcon = decimalformat.format(NewConfirmed_)
                    NewConfirmed.text = "+ $newcon"

                    val TotalConfirmed_ = global.getInt("TotalConfirmed")
                    val totalcon = decimalformat.format(TotalConfirmed_)
                    TotalConfirmed.text = "+ $totalcon"

                    val NewDeaths_ = global.getInt("NewDeaths")
                    val newdeath = decimalformat.format(NewDeaths_)
                    NewDeaths.text = "+ $newdeath"

                    val TotalDeaths_ = global.getInt("TotalDeaths")
                    val totaldeath = decimalformat.format(TotalDeaths_)
                    TotalDeaths.text = "+ $totaldeath"

                    val NewRecovered_ = global.getInt("NewRecovered")
                    val newrec = decimalformat.format(NewRecovered_)
                    NewRecovered.text = "+ $newrec"

                    val TotalRecovered_ = global.getInt("TotalRecovered")
                    val totalrec = decimalformat.format(TotalRecovered_)
                    TotalRecovered.text = "+ $totalrec"

                    paren.visibility = View.VISIBLE
                    spnkit.visibility = View.GONE


                }catch (e:JSONException){

                    e.printStackTrace()
                }

                },
                {

                })

        volleyrequest!!.add(jsonobject)
    }

}