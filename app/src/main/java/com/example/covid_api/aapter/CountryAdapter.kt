package com.example.covid_api.aapter


import android.content.Context
import android.content.Intent

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.covid_api.ChartActivity
import com.example.covid_api.R
import com.example.covid_api.model.Countris
import java.text.DecimalFormat


class CountryAdapter(val context: Context,var list: ArrayList<Countris> ): RecyclerView.Adapter<CountryAdapter.myviewholder>() {

    class myviewholder(itemView: View) :RecyclerView.ViewHolder(itemView){

        var NewConfirmed = itemView.findViewById<TextView>(R.id.item_NewConfirmed_id)
        var NewDeaths = itemView.findViewById<TextView>(R.id.item_NewDeaths_id)
        var NewRecovered = itemView.findViewById<TextView>(R.id.item_NewRecovered_id)
        var TotalConfirmed = itemView.findViewById<TextView>(R.id.item_TotalConfirmed_id)
        var TotalDeaths = itemView.findViewById<TextView>(R.id.item_TotalDeaths_id)
        var TotalRecovered = itemView.findViewById<TextView>(R.id.item_TotalRecovered_id)
        var name = itemView.findViewById<TextView>(R.id.item_country_name_id)



        override fun toString(): String {
            return super.toString()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myviewholder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_country , parent , false)
        return myviewholder(view)
    }

    override fun onBindViewHolder(holder: myviewholder, position: Int) {
       val decimalformat = DecimalFormat("###,###")


        val newcon = list[position].NewConfirmed
        holder.NewConfirmed.text = decimalformat.format(Integer.valueOf(newcon))

        val newdeath = list[position].NewDeaths
        holder.NewDeaths.text = decimalformat.format(Integer.valueOf(newdeath))

        val newrec = list[position].NewRecovered
        holder.NewRecovered.text = decimalformat.format(Integer.valueOf(newrec))

        val totalcon = list[position].TotalConfirmed
        holder.TotalConfirmed.text = decimalformat.format(Integer.valueOf(totalcon))

        val totaldeath = list[position].TotalDeaths
        holder.TotalDeaths.text = decimalformat.format(Integer.valueOf(totaldeath))

        val totalrec = list[position].TotalRecovered
        holder.TotalRecovered.text = decimalformat.format(Integer.valueOf(totalrec))

        holder.name.text = list[position].Country

        holder.itemView.setOnClickListener {

            val intent = Intent(context , ChartActivity::class.java)

            intent.putExtra("NewConfirmed" , list[position].NewConfirmed)
            intent.putExtra("NewDeaths" , list[position].NewDeaths)
            intent.putExtra("NewRecovered" , list[position].NewRecovered)
            intent.putExtra("TotalConfirmed" , list[position].TotalConfirmed)
            intent.putExtra("TotalDeaths" , list[position].TotalDeaths)
            intent.putExtra("TotalRecovered" , list[position].TotalRecovered)
            intent.putExtra("name" , list[position].Country)

            context.startActivity(intent)

        }


    }

    override fun getItemCount(): Int {
        return list.size
    }
    // ست کردن لیست جدید کشورها
    fun filterlist(filteredlist:ArrayList<Countris>){

        list = filteredlist
        notifyDataSetChanged()
    }


    }
