package com.example.covid_api.model

class Countris(
    var Country:String ,
    var CountryCode:String ,
    val Slug:String,
    var NewConfirmed:String,
    var TotalConfirmed:String ,
    var NewDeaths:String ,
    var TotalDeaths:String ,
    var NewRecovered:String ,
    var TotalRecovered:String ,
    var Date:String
)