package com.example.covid_api.model

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import com.example.covid_api.R

class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        supportActionBar!!.hide()

        val back = findViewById<TextView>(R.id.country_about_back_id)

        back.setOnClickListener {

            finish()
        }

        val btn_tel = findViewById<TextView>(R.id.about_tel_id)

        btn_tel.text = "@salah_azami"

        btn_tel.setOnClickListener {

            val appname:String = "org.telegram.messenger"
            val isappinstalled:Boolean = installpackmanager(applicationContext , appname)

            if (isappinstalled){
                val url = "https://t.me/salah_azami"
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setData(Uri.parse(url))
                startActivity(intent)

            }else{

                Toast.makeText(applicationContext,"تلگرام روی گوشی شما نصب نمی باشد!",Toast.LENGTH_LONG).show()
            }

        }
    }

    // در این فانکشن ما بررسی میکنیم که آیا اپلیکیشنی که ما می خوایم واردش بشیم روی گوشی نصب هست یا نه
    fun installpackmanager(context: Context , name:String):Boolean{

        val pm:PackageManager = context.packageManager

        try {
            pm.getPackageInfo(name , PackageManager.GET_ACTIVITIES)

            return true
        }catch (e:PackageManager.NameNotFoundException){
            return false
        }


    }
}